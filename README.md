# Pixel board firmware
## Usage

Write `cargo xtask` for a list of available compilation/running tasks.

## Structure based on:

Example of testing an embedded application with 3 different kinds of tests.
[Check out the associated blog post here!](https://ferrous-systems.com/blog/test-embedded-app/)


## Installing the toolchain and compile

(tested in Debian 12)
Please refer to https://github.com/knurling-rs/app-template for the installation instructions.

To set up the Rust environment for a ESP32 C3 based board, you can follow the guide in the Embedded Rust on Espressif (https://esp-rs.github.io/std-training/02_2_software.html) or The Rust on ESP Book (https://esp-rs.github.io/book/installation/index.html). In a nutshell:

```commandline
sudo apt-get install curl libudev-dev pkg-config
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
cargo install cargo-espflash espflash ldproxy
sudo apt install llvm-dev libclang-dev clang
```