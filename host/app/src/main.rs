//! # Example: Hello world
//!
//! A simple hello world example displaying some primitive shapes and some text underneath.

use std::thread::sleep;
use std::time::Duration;

use embedded_graphics::{
    mono_font::{ascii::FONT_4X6, MonoTextStyle},
    pixelcolor::Rgb888,
    prelude::*,
    primitives::{Circle, PrimitiveStyle, Rectangle, Triangle},
    text::{Alignment, Text},
};
use embedded_graphics_simulator::{
    OutputSettingsBuilder, SimulatorDisplay, SimulatorEvent, Window,
};

mod screen;
use crate::screen::get_screen_manager;

use app_configuration::print_current_censored_config;


fn main() -> Result<(), std::convert::Infallible> {
    let mut screen_ = get_screen_manager().unwrap();

    let output_settings = OutputSettingsBuilder::new()
        .scale(20)
        .pixel_spacing(0)
        .build();
    let mut window = Window::new("Main", &output_settings);

    print_current_censored_config();

    loop {
        //draw.clear_with_black().unwrap();
        let _ = screen_.draw_handler.clear(Rgb888::CSS_ANTIQUE_WHITE);

        //let mut translated_draw = draw.translated(Point::zero());
        let mut translated_draw = screen_.draw_handler.translated(Point::zero());

        let character_style = MonoTextStyle::new(&FONT_4X6, Rgb888::CSS_BLUE_VIOLET);
        let text = "Hi there";
        Text::with_alignment(text, Point::new(14, 5), character_style, Alignment::Center)
            .draw(&mut translated_draw)
            .unwrap();

        window.update(&screen_.draw_handler);

        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break;
        }

        sleep(Duration::from_millis(1000));
    }
    Ok(())
}
