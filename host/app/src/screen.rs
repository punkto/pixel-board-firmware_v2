use embedded_graphics::prelude::*;

use embedded_graphics_simulator::SimulatorDisplay;

use embedded_graphics::pixelcolor::Rgb888;

use screen_manager::{ScreenError, ScreenManager};

pub fn get_screen_manager() -> Result<ScreenManager<SimulatorDisplay<Rgb888>>, ScreenError> {
    const MAX_X: u32 = 28;
    const MAX_Y: u32 = 8;

    let display: SimulatorDisplay<Rgb888> = SimulatorDisplay::new(Size::new(MAX_X, MAX_Y));

    Ok(ScreenManager {
        draw_handler: display,
    })
}
