//! # Example: OSWHDEM demo
//!
//! A simple hello world example for the OSWHDEM.

// usage: cargo run --example oshwdem_demo

use std::thread::sleep;
use std::time::Duration;

use embedded_graphics::{pixelcolor::Rgb888, prelude::*};
use embedded_graphics_simulator::{
    OutputSettingsBuilder, SimulatorDisplay, SimulatorEvent, Window,
};

use screen_manager::ScreenManager;

fn main() -> Result<(), std::convert::Infallible> {
    const MAX_X: u32 = 28;
    const MAX_Y: u32 = 8;

    let display: SimulatorDisplay<Rgb888> = SimulatorDisplay::new(Size::new(MAX_X, MAX_Y));
    let mut screen_manager = ScreenManager::init(display).unwrap();

    let output_settings = OutputSettingsBuilder::new()
        .scale(40)
        .pixel_spacing(0)
        .build();
    let mut window = Window::new("oshwdem demo", &output_settings);

    println!("Start!");

    let texts: [TextData; 2] = [
        TextData {
            text: "a",
            start_x: 5,
            color: Rgb888::CSS_BLUE_VIOLET,
        },
        TextData {
            text: "Industriosa",
            start_x: 9,
            color: Rgb888::CSS_BURLY_WOOD,
        },
    ];
    print_moving_texts(&mut screen_manager, &texts, &mut window, 60, 20);

    print_moving_text(
        &mut screen_manager,
        "hola!",
        10,
        Rgb888::RED,
        &mut window,
        10,
        1,
    );
    sleep(Duration::from_millis(5000));

    let texts: [TextData; 2] = [
        TextData {
            text: "oswhdem",
            start_x: 5,
            color: Rgb888::CSS_BLUE_VIOLET,
        },
        TextData {
            text: "2023",
            start_x: 35,
            color: Rgb888::BLUE,
        },
    ];
    print_moving_texts(&mut screen_manager, &texts, &mut window, 90, 30);

    let mut x: usize = 0;
    let mut y: usize = 0;
    let mut color = Rgb888::CSS_DARK_MAGENTA;
    loop {
        println!("x: {} y: {}", x, y);

        let _ = screen_manager.draw_handler.clear(Rgb888::CSS_ANTIQUE_WHITE);
        screen_manager.print_text("OSHWDEM", 0, Rgb888::BLUE);
        screen_manager.print_rectangle(Point::new(x as i32, y as i32), Size::new(1, 1), color);
        window.update(&screen_manager.draw_handler);

        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break;
        }

        y += 1;
        if y >= MAX_Y as usize {
            y = 0;
            x += 1;
            color = Rgb888::new(color.b(), color.r(), color.g());
            if x >= MAX_X as usize - 1 {
                x = 0;
            }
        }

        sleep(Duration::from_millis(1000));
    }
    Ok(())
}

fn print_moving_text(
    screen_manager: &mut ScreenManager<SimulatorDisplay<Rgb888>>,
    text: &str,
    start_x: i32,
    color: Rgb888,
    window: &mut Window,
    steps: u32,
    delay_millis: u64,
) {
    for i in 0..steps {
        let _ = screen_manager.draw_handler.clear(Rgb888::CSS_ANTIQUE_WHITE);
        screen_manager.print_text(text, start_x - i as i32, color);
        window.update(&screen_manager.draw_handler);
        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break;
        }
        sleep(Duration::from_millis(delay_millis));
    }
}

#[derive(Debug)]
struct TextData<'a> {
    text: &'a str,
    start_x: i32,
    color: Rgb888,
}

fn print_moving_texts(
    screen_manager: &mut ScreenManager<SimulatorDisplay<Rgb888>>,
    texts: &[TextData],
    window: &mut Window,
    steps: u32,
    delay_millis: u64,
) {
    for i in 0..steps {
        let _ = screen_manager.draw_handler.clear(Rgb888::CSS_ANTIQUE_WHITE);
        for text_data in texts {
            screen_manager.print_text(
                text_data.text,
                text_data.start_x - i as i32,
                text_data.color,
            );
        }
        window.update(&screen_manager.draw_handler);
        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break;
        }
        sleep(Duration::from_millis(delay_millis));
    }
}
