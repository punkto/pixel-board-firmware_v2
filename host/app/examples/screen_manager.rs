//! # Example: Screen manager
//!
//! A simple hello world example for the screen manager.

// usage: cargo run --example screen_manager


use std::thread::sleep;
use std::time::Duration;

use embedded_graphics::{pixelcolor::Rgb888, prelude::*};
use embedded_graphics_simulator::{
    OutputSettingsBuilder, SimulatorDisplay, SimulatorEvent, Window,
};

use screen_manager::ScreenManager;

fn main() -> Result<(), std::convert::Infallible> {
    const MAX_X: u32 = 28;
    const MAX_Y: u32 = 8;

    let display: SimulatorDisplay<Rgb888> = SimulatorDisplay::new(Size::new(MAX_X, MAX_Y));
    let mut screen_manager = ScreenManager::init(display).unwrap();

    let output_settings = OutputSettingsBuilder::new()
        .scale(40)
        .pixel_spacing(0)
        .build();
    let mut window = Window::new("Screen manager example", &output_settings);

    println!("Start!");

    let mut x: usize = 0;
    let mut y: usize = 0;
    let mut color = Rgb888::CSS_DARK_MAGENTA;
    loop {
        println!("x: {} y: {}", x, y);

        let _ = screen_manager.draw_handler.clear(Rgb888::CSS_ANTIQUE_WHITE);

        screen_manager.print_text("ola", 3, Rgb888::BLUE);
        screen_manager.print_rectangle(Point::new(x as i32, y as i32), Size::new(1, 1), color);

        window.update(&screen_manager.draw_handler);

        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break;
        }

        y += 1;
        if y >= MAX_Y as usize {
            y = 0;
            x += 1;
            color = Rgb888::new(color.b(), color.r(), color.g());
            if x >= MAX_X as usize - 1 {
                x = 0;
            }
        }

        sleep(Duration::from_millis(1000));
    }
    Ok(())
}
