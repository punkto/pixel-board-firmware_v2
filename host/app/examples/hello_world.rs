//! # Example: Hello world
//!
//! A simple hello world example displaying some primitive shapes and some text underneath.

use std::thread::sleep;
use std::time::Duration;

use embedded_graphics::{
    mono_font::{ascii::FONT_4X6, MonoTextStyle},
    pixelcolor::Rgb888,
    prelude::*,
    primitives::{
        Circle, PrimitiveStyle, Rectangle, Triangle,
    },
    text::{Alignment, Text},
};
use embedded_graphics_simulator::{
    SimulatorEvent, OutputSettingsBuilder, SimulatorDisplay, Window,
};

fn main() -> Result<(), std::convert::Infallible> {
    const MAX_X: u32 = 28;
    const MAX_Y: u32 = 8;

    let mut display: SimulatorDisplay<Rgb888> = SimulatorDisplay::new(Size::new(MAX_X, MAX_Y));

    //display.set_brightness(40);

    let output_settings = OutputSettingsBuilder::new().scale(20).pixel_spacing(0).build();
    let mut window = Window::new("Sub images", &output_settings);

    println!("Start Ws2812DrawTarget!");

    let mut x: usize = 0;
    let mut y: usize = 0;
    let mut color = Rgb888::CSS_DARK_MAGENTA;
    loop {
        println!("x: {} y: {}", x, y);

        //draw.clear_with_black().unwrap();
        let _ = display.clear(Rgb888::CSS_ANTIQUE_WHITE);

        //let mut translated_draw = draw.translated(Point::zero());
        let mut translated_draw = display.translated(Point::zero());

        Rectangle::new(Point::new(x as i32, y as i32), Size::new(1, 1))
            .into_styled(PrimitiveStyle::with_fill(color))
            .draw(&mut translated_draw)
            .unwrap();

        Rectangle::new(Point::new(0, 0), Size::new(2, 2))
            .into_styled(PrimitiveStyle::with_fill(Rgb888::CSS_ORANGE))
            .draw(&mut translated_draw)
            .unwrap();

        Circle::new(Point::new(20, 3), 4)
            .into_styled(PrimitiveStyle::with_fill(Rgb888::BLUE))
            .draw(&mut translated_draw)
            .unwrap();

        Triangle::new(Point::new(24, 4), Point::new(24, 5), Point::new(26, 3))
            .into_styled(PrimitiveStyle::with_fill(Rgb888::GREEN))
            .draw(&mut translated_draw)
            .unwrap();

        let character_style = MonoTextStyle::new(&FONT_4X6, Rgb888::CSS_BLUE_VIOLET);

        let text = "Hi there";
        Text::with_alignment(text, Point::new(14, 5), character_style, Alignment::Center)
            .draw(&mut translated_draw)
            .unwrap();

        //draw.flush().unwrap();

        window.update(&display);

        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break;
        }

        sleep(Duration::from_millis(1000));

        y += 1;
        if y >= MAX_Y as usize {
            y = 0;
            x += 1;
            color = Rgb888::new(color.b(), color.r(), color.g());
            //println!("new color {:?}", color);
            if x >= MAX_X as usize - 1 {
                x = 0;
            }
        }
    }
    Ok(())
}
