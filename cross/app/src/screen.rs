use embedded_graphics::prelude::*;

use ws2812_esp32_rmt_driver::lib_embedded_graphics::{
    LedPixelDrawTarget, LedPixelShape, Ws2812DrawTarget,
};

use screen_manager::{ScreenError, ScreenManager};


pub fn get_screen_manager() -> Result<ScreenManager::<LedPixelDrawTarget<embedded_graphics::pixelcolor::Rgb888, ws2812_esp32_rmt_driver::driver::color::LedPixelColorImpl<3, 1, 0, 2, 255>, LedPixelMatrixZigZagInverse<28, 8, 7>>>, ScreenError> {
        // The led matrix used in this example was initially 8x32 but some pixels at the start died, leaving the first 4 columns unusable but with 7
        // working pixels in them. We sacrifice those 7 good pixels from the start so the matrix can be considered
        // rectangular again.
        const MAX_X: usize = 28;
        const MAX_Y: usize = 8;
        const BLANK_START_PIXELS: usize = 7;

        let mut draw = Ws2812DrawTarget::<
            LedPixelMatrixZigZagInverse<MAX_X, MAX_Y, BLANK_START_PIXELS>,
        >::new(0, 7)
        .unwrap();
        draw.set_brightness(15);

        println!("Start Ws2812DrawTarget!");
        Ok(ScreenManager { draw_handler: draw })
}

pub struct LedPixelMatrixZigZagInverse<const W: usize, const H: usize, const BLANK_START_PIXELS: usize>
{}

impl<const W: usize, const H: usize, const BLANK_START_PIXELS: usize> LedPixelShape
    for LedPixelMatrixZigZagInverse<W, H, BLANK_START_PIXELS>
{
    fn size() -> Size {
        Size::new((W + 1) as u32, H as u32)
    }

    fn pixel_index(point: Point) -> Option<usize> {
        let w = (W + 1) as i32;
        let h = H as i32;

        if point.x < 0 || point.y < 0 || point.x >= w - 1 || point.y >= h {
            //println!("pixel_index: out of bounds!");
            return None;
        }

        let x = w - point.x - 2;
        let y = h - point.y - 1;

        //println!("pixel_index: x: {} y: {}", x, y);

        if x % 2 == 0 {
            let index = y + (x * h);
            //println!("pixel_index: index: {}", index);
            Some(index as usize + BLANK_START_PIXELS)
        } else {
            let index = (h - y - 1) + (x * h);
            //println!("pixel_index: index: {}", index);
            Some(index as usize + BLANK_START_PIXELS)
        }
    }
}
