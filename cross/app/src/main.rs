use esp_idf_sys::{self as _}; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

mod device_info;
mod network_manager;
mod screen;

use embedded_graphics::pixelcolor::Rgb888;
use embedded_graphics::prelude::{Point, RgbColor, Size, WebColors};

use esp_idf_hal::prelude::*;

use esp_idf_svc::eventloop::EspSystemEventLoop;
use esp_idf_svc::log::{set_target_level, EspLogger};

use log::*;

use std::thread::sleep;
use std::time;
use std::time::Duration;

use app_configuration::{print_current_censored_config, CURRENT_CONFIG};
use crate::device_info::{
    get_device_mac, get_firmware_date, get_firmware_time, get_firmware_version,
};
use night_checker::{get_night_checker_data, NightCheckerData};
use crate::screen::{get_screen_manager};

fn main() -> ! {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();
    let peripherals = Peripherals::take().unwrap();

    let mut screen = get_screen_manager().unwrap();

    EspLogger::initialize_default();
    set_target_level("*", LevelFilter::Warn).unwrap(); // This can be set to LevelFilter::Off for no messages (except from the bootloader)
    set_target_level("rust-logging", LevelFilter::Debug).unwrap(); // This can be set to LevelFilter::Off for no messages (except from the bootloader)

    let sysloop = EspSystemEventLoop::take().unwrap();

    print_log_data();
    //update_clock(peripherals, sysloop);

    do_pixel_board(screen);

    loop {
        error!("Code shouldn't reach this");
        sleep(Duration::from_millis(1000));
    }
}

fn print_log_data() {
    println!(
        "Device with mac {:?}. Firmware version {:?}. Compiled at {:?} {:?}",
        get_device_mac(),
        get_firmware_version(),
        get_firmware_date(),
        get_firmware_time()
    );

    let wakeup_reason = esp_idf_hal::reset::WakeupReason::get();
    println!("Wakeup reason: {:?}", wakeup_reason);
    let reset_reason = esp_idf_hal::reset::ResetReason::get();
    println!("Reset reason: {:?}", reset_reason);

    let start_time = time::SystemTime::now();
    println!("Start time: {:?}", start_time);

    print_current_censored_config();
}

fn update_clock(
    peripherals: Peripherals,
    sysloop: esp_idf_svc::eventloop::EspEventLoop<esp_idf_svc::eventloop::System>,
) {
    let mut networking =
        match network_manager::NetworkManager::get_network_manager(peripherals.modem, sysloop) {
            Ok(n) => {
                info!("Got network manager");
                n
            }
            Err(e) => panic!("NetworkManager error: {:?}", e),
        };

    let mut connected = false;
    while !connected {
        connected = match networking.connect_to_wifi(true) {
            Ok(_) => {
                let _ = network_manager::sync_ntp();
                true
            }
            Err(e) => {
                error!("Couldn't connect to WiFi {:?}", e);
                sleep(Duration::from_millis(1000));
                false
            }
        };
    }

    networking.disconnect().unwrap();
}
