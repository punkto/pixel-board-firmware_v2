// cargo run --example clock

// Example of wall clock

use esp_idf_sys::{self as _}; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

use embedded_graphics::pixelcolor::Rgb888;
use embedded_graphics::prelude::{Point, RgbColor, Size, WebColors};

use esp_idf_hal::prelude::*;

use esp_idf_svc::eventloop::EspSystemEventLoop;
use esp_idf_svc::log::{set_target_level, EspLogger};

use log::*;

use std::thread::sleep;
use std::time;
use std::time::Duration;

use night_checker::{get_night_checker_data, NightCheckerData};

use app_configuration::{print_current_censored_config, CURRENT_CONFIG};
use pixel_board_firmware::device_info::{
    get_device_mac, get_firmware_date, get_firmware_time, get_firmware_version,
};
use pixel_board_firmware::network_manager::{sync_ntp, NetworkManager};
use pixel_board_firmware::screen::get_screen_manager;
use screen_manager::ScreenManager;

fn main() -> () {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();
    let peripherals = Peripherals::take().unwrap();

    EspLogger::initialize_default();
    set_target_level("*", LevelFilter::Warn).unwrap(); // This can be set to LevelFilter::Off for no messages (except from the bootloader)
    set_target_level("rust-logging", LevelFilter::Debug).unwrap(); // This can be set to LevelFilter::Off for no messages (except from the bootloader)

    print_log_data();

    let mut screen = get_screen_manager().unwrap();

    let texts_a_industriosa: [TextData; 2] = [
        TextData {
            text: "a",
            start_x: 0,
            color: Rgb888::CSS_BLUE_VIOLET,
            velocity: 1,
        },
        TextData {
            text: "Industriosa",
            start_x: 6,
            color: Rgb888::CSS_GOLD,
            velocity: 1,
        },
    ];
    
    let texts_hola: [TextData; 1] = [TextData {
        text: "Hola!",
        start_x: -10,
        color: Rgb888::CSS_AQUA,
        velocity: -2,
    }];

    let texts_oswhdem_2023: [TextData; 2] = [
        TextData {
            text: "oswhdem",
            start_x: 0,
            color: Rgb888::CSS_BLUE_VIOLET,
            velocity: 1,
        },
        TextData {
            text: "2023",
            start_x: 35,
            color: Rgb888::BLUE,
            velocity: 1,
        },
    ];
    
    print_moving_texts(&mut screen, &texts_hola, 9, 1);

    let sysloop = EspSystemEventLoop::take().unwrap();
    update_clock(peripherals, sysloop);

    let app_config = { CURRENT_CONFIG.lock().unwrap().clone() };

    loop {
        print_moving_texts(&mut screen, &texts_a_industriosa, 1, 2000);
        print_moving_texts(&mut screen, &texts_a_industriosa, 60, 200);
        print_moving_texts(&mut screen, &texts_oswhdem_2023, 1, 2000);
        print_moving_texts(&mut screen, &texts_oswhdem_2023, 70, 200);

        let loops_to_reset = 30;
        let hours_to_add = 2;
        for _ in 0..loops_to_reset {
            let now = time::SystemTime::now();
            let mut night_checker_data =
                get_night_checker_data(now, app_config.latitude, app_config.longitude).unwrap_or(
                    // Couldn't get sunrise data. Force sampling by setting night time.
                    NightCheckerData {
                        is_night_time: true,
                        ..Default::default()
                    },
                );
            night_checker_data.hour = (night_checker_data.hour + hours_to_add) % 24;

            println!("{:?}", night_checker_data);

            let _ = screen.draw_handler.clear_with_black();
            print_clock(&mut screen, night_checker_data);

            let _ = screen.draw_handler.flush();

            sleep(Duration::from_millis(1000));
        }
    }
}

fn print_log_data() {
    println!(
        "Device with mac {:?}. Firmware version {:?}. Compiled at {:?} {:?}",
        get_device_mac(),
        get_firmware_version(),
        get_firmware_date(),
        get_firmware_time()
    );

    let wakeup_reason = esp_idf_hal::reset::WakeupReason::get();
    println!("Wakeup reason: {:?}", wakeup_reason);
    let reset_reason = esp_idf_hal::reset::ResetReason::get();
    println!("Reset reason: {:?}", reset_reason);

    let start_time = time::SystemTime::now();
    println!("Start time: {:?}", start_time);

    print_current_censored_config();
}

fn print_clock<T: embedded_graphics::draw_target::DrawTarget<Color = Rgb888>>(
    screen: &mut ScreenManager<T>,
    night_checker_data: NightCheckerData,
) {
    let colon_color = Rgb888::CSS_SLATE_GRAY;
    let hours_color = Rgb888::CSS_ORANGE_RED;
    let minutes_color = hours_color;
    let seconds_color = Rgb888::CSS_INDIGO;
    screen.clear();
    screen.flush();

    screen.print_text(
        format!("{:02}", night_checker_data.hour).as_str(),
        0,
        hours_color,
    );

    screen.print_text(":", 7, colon_color);

    screen.print_text(
        format!("{:02}", night_checker_data.minutes).as_str(),
        10,
        minutes_color,
    );

    if night_checker_data.seconds % 2 == 0 {
        screen.print_text(":", 17, colon_color);
    }

    screen.print_text(
        format!("{:02}", night_checker_data.seconds).as_str(),
        20,
        seconds_color,
    );

    let width: u32 = ((night_checker_data.hour as f32) / 24.0 * 8.0) as u32;
    screen.print_rectangle(Point::new(0_i32, 7_i32), Size::new(width, 1), Rgb888::BLUE);

    let width: u32 = ((night_checker_data.minutes as f32) / 60.0 * 8.0) as u32;
    screen.print_rectangle(
        Point::new(10_i32, 7_i32),
        Size::new(width, 1),
        Rgb888::GREEN,
    );

    let width: u32 = ((night_checker_data.seconds as f32) / 60.0 * 8.0) as u32;
    screen.print_rectangle(Point::new(20_i32, 7_i32), Size::new(width, 1), Rgb888::RED);

    screen.flush();
}

fn update_clock(
    peripherals: Peripherals,
    sysloop: esp_idf_svc::eventloop::EspEventLoop<esp_idf_svc::eventloop::System>,
) {
    let mut networking = match NetworkManager::get_network_manager(peripherals.modem, sysloop) {
        Ok(n) => {
            info!("Got network manager");
            n
        }
        Err(e) => panic!("NetworkManager error: {:?}", e),
    };

    let mut connected = false;
    while !connected {
        connected = match networking.connect_to_wifi(true) {
            Ok(_) => {
                let _ = sync_ntp();
                true
            }
            Err(e) => {
                error!("Couldn't connect to WiFi {:?}", e);
                sleep(Duration::from_millis(1000));
                false
            }
        };
    }

    networking.disconnect().unwrap();
}

#[derive(Debug)]
struct TextData<'a> {
    text: &'a str,
    start_x: i32,
    color: Rgb888,
    velocity: i32,
}

fn print_moving_texts(
    screen_manager: &mut ScreenManager<
        ws2812_esp32_rmt_driver::lib_embedded_graphics::LedPixelDrawTarget<
            Rgb888,
            ws2812_esp32_rmt_driver::driver::color::LedPixelColorImpl<3, 1, 0, 2, 255>,
            pixel_board_firmware::screen::LedPixelMatrixZigZagInverse<28, 8, 7>,
        >,
    >,
    texts: &[TextData],
    steps: u32,
    delay_millis: u64,
) {
    for i in 0..steps {
        let _ = screen_manager.draw_handler.clear_with_black();
        for text_data in texts {
            screen_manager.print_text(
                text_data.text,
                text_data.start_x - (i as i32 * text_data.velocity),
                text_data.color,
            );
        }
        let _ = screen_manager.draw_handler.flush();
        sleep(Duration::from_millis(delay_millis));
    }
}
