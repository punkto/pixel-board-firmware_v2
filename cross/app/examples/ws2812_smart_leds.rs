// cargo run --example ws2812_smart_leds

// based on
// https://github.com/cat-in-136/ws2812-esp32-rmt-driver/blob/main/examples/m5atom_smart_leds.rs

use esp_idf_sys::*;
use smart_leds::hsv::{hsv2rgb, Hsv};
// in the original, they use 'use smart_leds_trait::SmartLedsWrite;' but this
// gives a 'use of undeclared crate or module `smart_leds_trait`'
use smart_leds::SmartLedsWrite;
use std::thread::sleep;
use std::time::Duration;
use ws2812_esp32_rmt_driver::Ws2812Esp32Rmt;

fn main() -> ! {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();

    // Ws2812Esp32Rmt is like a led strip. You can write a iterator of RGB pixels on it,
    // that would be the smart_leds::SmartLedsWrite trait.
    let mut ws2812 = Ws2812Esp32Rmt::new(0, 7).unwrap();

    println!("Start NeoPixel rainbow!");

    let mut hue = unsafe { esp_random() } as u8;
    loop {
        let pixels = std::iter::repeat(hsv2rgb(Hsv {
            hue,
            sat: 255,
            val: 2,
        }))
        .take(103);
        ws2812.write(pixels).unwrap();

        sleep(Duration::from_millis(100));

        hue = hue.wrapping_add(1);
    }
}
