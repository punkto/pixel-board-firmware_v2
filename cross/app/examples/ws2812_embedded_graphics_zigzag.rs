// cargo run --example ws2812_embedded_graphics_zigzag

// Example of implementation of a LedPixelShape for a led matrix that goes in zig zag like these:
// https://www.amazon.es/Longruner-Integrado-Flexible-Iluminaci%C3%B3n-Completo/dp/B07KT1H481
// https://www.adafruit.com/product/2294

// based on
// https://github.com/cat-in-136/ws2812-esp32-rmt-driver/blob/main/examples/m5atom_embedded_graphics.rs

use embedded_graphics::mono_font::{ascii::FONT_4X6, MonoTextStyle};
use embedded_graphics::pixelcolor::Rgb888;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::{Circle, PrimitiveStyle, Rectangle, Triangle};
use embedded_graphics::text::{Alignment, Text};
use std::thread::sleep;
use std::time::Duration;
use ws2812_esp32_rmt_driver::lib_embedded_graphics::{LedPixelShape, Ws2812DrawTarget};

pub struct LedPixelMatrixZigZag<const W: usize, const H: usize, const BLANK_START_PIXELS: usize> {
}

impl<const W: usize, const H: usize, const BLANK_START_PIXELS: usize> LedPixelShape
    for LedPixelMatrixZigZag<W, H, BLANK_START_PIXELS>
{
    fn size() -> Size {
        Size::new((W + 1) as u32, H as u32)
    }

    fn pixel_index(point: Point) -> Option<usize> {
        let w = (W + 1) as i32;
        let h = H as i32;

        if point.x < 0 || point.y < 0 || point.x >= w - 1 || point.y >= h {
            //println!("pixel_index: out of bounds!");
            return None;
        }

        let x = point.x;
        let y = point.y;

        //println!("pixel_index: x: {} y: {}", x, y);

        if x % 2 == 0 {
            let index = y + (x * h);
            //println!("pixel_index: index: {}", index);
            Some(index as usize + BLANK_START_PIXELS)
        } else {
            let index = (h - y - 1) + (x * h);
            //println!("pixel_index: index: {}", index);
            Some(index as usize + BLANK_START_PIXELS)
        }
    }
}

fn main() -> ! {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();

    // The led matrix used in this example was initially 8x32 but some pixels at the start died, leaving the first 4 columns unusable but with 7
    // working pixels in them. We sacrifice those 7 good pixels from the start so the matrix can be considered
    // rectangular again.
    const MAX_X: usize = 28;
    const MAX_Y: usize = 8;
    const BLANK_START_PIXELS: usize = 7;

    let mut draw =
        Ws2812DrawTarget::<LedPixelMatrixZigZag<MAX_X, MAX_Y, BLANK_START_PIXELS>>::new(0, 7)
            .unwrap();
    draw.set_brightness(40);

    println!("Start Ws2812DrawTarget!");

    let mut x: usize = 0;
    let mut y: usize = 0;
    let mut color = Rgb888::CSS_DARK_MAGENTA;
    loop {
        println!("x: {} y: {}", x, y);

        draw.clear_with_black().unwrap();

        let mut translated_draw = draw.translated(Point::zero());

        Rectangle::new(Point::new(x as i32, y as i32), Size::new(1, 1))
            .into_styled(PrimitiveStyle::with_fill(color))
            .draw(&mut translated_draw)
            .unwrap();

        Rectangle::new(Point::new(0, 0), Size::new(2, 2))
            .into_styled(PrimitiveStyle::with_fill(Rgb888::CSS_ORANGE))
            .draw(&mut translated_draw)
            .unwrap();

        Circle::new(Point::new(20, 3), 4)
            .into_styled(PrimitiveStyle::with_fill(Rgb888::BLUE))
            .draw(&mut translated_draw)
            .unwrap();

        // Triangle::new(Point::new(24, 4), Point::new(24, 5), Point::new(26, 3))
        //     .into_styled(PrimitiveStyle::with_fill(Rgb888::GREEN))
        //     .draw(&mut translated_draw)
        //     .unwrap();

        let character_style = MonoTextStyle::new(&FONT_4X6, Rgb888::CSS_BLUE_VIOLET);

        let text = "Hi there";
        Text::with_alignment(text, Point::new(14, 5), character_style, Alignment::Center)
            .draw(&mut translated_draw)
            .unwrap();

        draw.flush().unwrap();

        y += 1;
        if y >= MAX_Y {
            y = 0;
            x += 1;
            color = Rgb888::new(color.b(), color.r(), color.g());
            //println!("new color {:?}", color);
            if x >= MAX_X - 1 {
                x = 0;
            }
        }

        sleep(Duration::from_millis(100));
    }
}
