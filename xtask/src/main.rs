#![allow(dead_code)]
#![deny(unused_must_use)]

use std::{env, path::PathBuf};

use xshell::{cmd, Shell};

fn main() -> Result<(), anyhow::Error> {
    let args = env::args().skip(1).collect::<Vec<_>>();
    let args = args.iter().map(|s| &**s).collect::<Vec<_>>();

    match &args[..] {
        ["clean", "target"] => clean_target(),
        ["release", "target"] => release_target(),
        ["release", "host"] => release_host(),
        ["test", "all"] => test_all(),
        ["test", "lib"] => test_lib(),
        ["test", "lib", "night_checker"] => test_lib_("night_checker"),
        ["test", "host"] => test_host(),
        ["test", "target"] => test_target(),
        ["example", "target-clock"] => test_example_target_clock(),
        ["example", "target-ws2812"] => test_example_target_ws2812(),
        ["example", "target-oshwdem"] => test_example_target_oshwdem(),
        ["example", "host-oshwdem"] => test_example_host_oshwdem(),
        _ => {
            println!("USAGE:");
            println!("\tcargo xtask clean target");
            println!("\tcargo xtask release target");
            println!("\tcargo xtask release host");
            println!("\tcargo xtask test all");
            println!("\tcargo xtask test lib");
            println!("\tcargo xtask test lib night_checker");
            println!("\tcargo xtask test host");
            println!("\tcargo xtask test target");
            println!("\tcargo xtask example target-clock");
            println!("\tcargo xtask example target-ws2812");
            println!("\tcargo xtask example target-oshwdem");
            println!("\tcargo xtask example host-oshwdem");
            Ok(())
        }
    }
}

fn test_all() -> Result<(), anyhow::Error> {
    test_host()?;
    test_target()
}

fn clean_target() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo clean").run()?;
    Ok(())
}

fn release_target() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo run --release").run()?;
    Ok(())
}

fn release_host() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("host").join("app"));
    cmd!(sh, "cargo run --release").run()?;
    Ok(())
}

fn test_host() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir());
    cmd!(sh, "cargo test --workspace").run()?;
    Ok(())
}

fn test_lib() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir());
    cmd!(sh, "cargo test --workspace --exclude cross --exclude host").run()?;
    Ok(())
}

fn test_lib_(lib_name: &str) -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("lib").join(lib_name));
    cmd!(sh, "cargo test -- --color always --show-output").run()?;
    Ok(())
}

fn test_target() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo run --example ws2812_embedded_graphics_zigzag_inverse").run()?;
    Ok(())
}

fn test_example_target_clock() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo clean").run()?;
    cmd!(sh, "cargo run --example clock").run()?;
    Ok(())
}

fn test_example_target_ws2812() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo run --example ws2812_embedded_graphics_zigzag_inverse").run()?;
    Ok(())
}

fn test_example_target_oshwdem() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo run --example oshwdem_demo").run()?;
    Ok(())
}

fn test_example_host_oshwdem() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("host").join("app"));
    cmd!(sh, "cargo run --example oshwdem_demo").run()?;
    Ok(())
}

fn flash() -> Result<(), anyhow::Error> {
    let sh = Shell::new()?;
    sh.change_dir(root_dir().join("cross").join("app"));
    cmd!(sh, "cargo flash --release").run()?;
    Ok(())
}

fn root_dir() -> PathBuf {
    let mut xtask_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    xtask_dir.pop();
    xtask_dir
}
