// Calculates a number of time related values given a time and a location.

//use sunrise::sunrise_sunset;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum NightCheckerError {
    #[error("SystemTime not valid")]
    SystemTimeError,
    #[error("Overflow when adding two dates")]
    DateError,
}

// Data calculated by this module, packed so it can be easily transferred.
#[derive(Debug, PartialEq)]
pub struct NightCheckerData {
    pub year: i32,
    pub month: u32,
    pub day: u32,
    pub hour: u8,
    pub minutes: u8,
    pub seconds: u8,
    pub now_ts: i64,
    pub sunrise_ts: i64,
    pub sunset_ts: i64,
    pub is_night_time: bool,
}

impl Default for NightCheckerData {
    fn default() -> Self {
        NightCheckerData {
            year: 0,
            month: 0,
            day: 0,
            hour: 0,
            minutes: 0,
            seconds: 0,
            now_ts: 0,
            sunrise_ts: 0,
            sunset_ts: 0,
            is_night_time: true,
        }
    }
}

// Calculates a number of time related values given a time and a location.
pub fn get_night_checker_data(
    time: std::time::SystemTime,
    _latitude: f64,
    _longitude: f64,
) -> Result<NightCheckerData, NightCheckerError> {
    let now_ts = match time.duration_since(std::time::UNIX_EPOCH) {
        Ok(time) => time.as_secs() as i64,
        Err(_) => {
            return Err(NightCheckerError::SystemTimeError);
        }
    };

    if now_ts < 30000 {
        return Err(NightCheckerError::SystemTimeError);
    }

    match time::PrimitiveDateTime::new(
        time::Date::from_ordinal_date(1970, 1).unwrap(),
        time::Time::MIDNIGHT,
    )
    .checked_add(time::Duration::new(now_ts, 0))
    {
        Some(datetime) => {
            let year: i32 = datetime.year();
            let month: u32 = datetime.month() as u32;
            let day: u32 = datetime.day() as u32;

            let hour = datetime.hour();
            let minutes = datetime.minute();
            let seconds = datetime.second();

            // the following line panics in the ESP32C3 because of https://github.com/esp-rs/esp-idf-sys/issues/235
            // let sunrise_sunset_ts = sunrise_sunset(latitude, longitude, year, month, day);
            let sunrise_sunset_ts = (0, 10);
            Ok(NightCheckerData {
                year,
                month,
                day,
                hour,
                minutes,
                seconds,
                now_ts,
                sunrise_ts: sunrise_sunset_ts.0,
                sunset_ts: sunrise_sunset_ts.1,
                is_night_time: (now_ts < sunrise_sunset_ts.0) || (sunrise_sunset_ts.1 < now_ts),
            })
        }
        None => Err(NightCheckerError::DateError),
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use crate::{get_night_checker_data, NightCheckerData};

    #[test]
    fn test_get_night_checker_data_works() {
        let seconds_in_a_year: f64 = 31_536_000.0;
        let time =
            std::time::SystemTime::UNIX_EPOCH + Duration::from_secs_f64(seconds_in_a_year * 35.0);
        let data = get_night_checker_data(time, 0.0, 0.0).unwrap();

        let expected = NightCheckerData {
            year: 2004,
            month: 12,
            day: 23,
            hour: 0,
            minutes: 0,
            seconds: 0,
            now_ts: 1103760000,
            sunrise_ts: 0,
            sunset_ts: 10,
            is_night_time: true,
        };
        assert_eq!(expected, data);
    }


    #[test]
    fn test_get_night_checker_data_works_for_current_time() {
        let time = std::time::SystemTime::now();
        let data = get_night_checker_data(time, 0.0, 0.0).unwrap();
        println!("{:?}", data);
        assert!(get_night_checker_data(time, 0.0, 0.0).is_ok());
    }
}
