use embedded_graphics::mono_font::{ascii::FONT_4X6, MonoTextStyle};
use embedded_graphics::pixelcolor::Rgb888;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::{PrimitiveStyle, Rectangle};
use embedded_graphics::text::{Alignment, Text};

use thiserror::Error;

pub struct ScreenManager<T: DrawTarget<Color = Rgb888>> {
    pub draw_handler: T,
}

#[derive(Debug, Error)]
pub enum ScreenError {
    #[error("Screen Error")]
    SomethingWrongWithScreen,
}

impl<T: DrawTarget<Color = Rgb888>> ScreenManager<T> {
    pub fn init(draw: T) -> Result<ScreenManager<T>, ScreenError> {
        Ok(ScreenManager { draw_handler: draw })
    }

    pub fn clear(&mut self) {
        //self.draw_handler.clear_with_black().unwrap();
    }

    pub fn flush(&mut self) {
        //self.draw_handler.flush().unwrap();
    }

    pub fn print_text(&mut self, text: &str, x: i32, color: Rgb888) {
        let mut translated_draw = self.draw_handler.translated(Point::zero());

        let character_style = MonoTextStyle::new(&FONT_4X6, color);

        match Text::with_alignment(text, Point::new(x, 5), character_style, Alignment::Left)
            .draw(&mut translated_draw)
        {
            Ok(_) => {}
            Err(_) => {}
        }
    }

    pub fn print_rectangle(&mut self, top_left: Point, size: Size, color: Rgb888) {
        let mut translated_draw = self.draw_handler.translated(Point::zero());

        match Rectangle::new(top_left, size)
            .into_styled(PrimitiveStyle::with_fill(color))
            .draw(&mut translated_draw)
        {
            Ok(_) => {}
            Err(_) => {}
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn crc_works() {
        // example from the Interface Specification document
        println!("Hi 34");
        assert_eq!(true, true);
    }
}
