use embedded_graphics::prelude::*;
use embedded_graphics::pixelcolor::Rgb888;
use screen_manager::ScreenManager;

pub struct PixelBoard {
    screen: ScreenManager<dyn DrawTarget<Color = Rgb888>>,
}

impl PixelBoard {
    pub fn do_pixel_board(&mut self) -> ! {
        self.print_salutation();

        let app_config = { CURRENT_CONFIG.lock().unwrap().clone() };

        loop {
            let now = time::SystemTime::now();
            let night_checker_data =
                get_night_checker_data(now, app_config.latitude, app_config.longitude).unwrap_or(
                    // Couldn't get sunrise data. Force sampling by setting night time.
                    NightCheckerData {
                        is_night_time: true,
                        ..Default::default()
                    },
                );

            println!("{:?}", night_checker_data);

            print_clock(&mut screen, night_checker_data);

            sleep(Duration::from_millis(1000));
        }
    }

    fn print_salutation(screen: &mut ScreenManager) {
        screen.clear();
        screen.print_text("Hola", 8, Rgb888::RED);
        screen.flush();
    }

    fn print_clock(screen: &mut ScreenManager, night_checker_data: NightCheckerData) {
        let colon_color = Rgb888::CSS_SLATE_GRAY;
        let hours_color = Rgb888::CSS_ORANGE_RED;
        let minutes_color = hours_color;
        let seconds_color = Rgb888::CSS_INDIGO;
        screen.clear();
        screen.flush();

        screen.print_text(
            format!("{:02}", night_checker_data.hour).as_str(),
            0,
            hours_color,
        );

        screen.print_text(":", 7, colon_color);

        screen.print_text(
            format!("{:02}", night_checker_data.minutes).as_str(),
            10,
            minutes_color,
        );

        if night_checker_data.seconds % 2 == 0 {
            screen.print_text(":", 17, colon_color);
        }

        screen.print_text(
            format!("{:02}", night_checker_data.seconds).as_str(),
            20,
            seconds_color,
        );

        let width: u32 = ((night_checker_data.hour as f32) / 24.0 * 8.0) as u32;
        screen.print_rectangle(Point::new(0_i32, 7_i32), Size::new(width, 1), Rgb888::BLUE);

        let width: u32 = ((night_checker_data.minutes as f32) / 60.0 * 8.0) as u32;
        screen.print_rectangle(
            Point::new(10_i32, 7_i32),
            Size::new(width, 1),
            Rgb888::GREEN,
        );

        let width: u32 = ((night_checker_data.seconds as f32) / 60.0 * 8.0) as u32;
        screen.print_rectangle(Point::new(20_i32, 7_i32), Size::new(width, 1), Rgb888::RED);

        screen.flush();
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn generic_test() {
        println!("Hi from the pixel_board tests");
        assert_eq!(true, true);
    }
}
