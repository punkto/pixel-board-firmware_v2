// object that maintains the configuration variables

// Singleton idea from http://oostens.me/posts/singletons-in-rust/

use once_cell::sync::Lazy;
use std::sync::Mutex;

const DEFAULT_LATITUDE: f64 = 42.23282;
const DEFAULT_LONGITUDE: f64 = -8.72264;

#[derive(Debug, Clone)]
pub struct AppConfiguration {
    pub external_wifi_ssid: String,
    pub external_wifi_psk: String,

    pub captive_wifi_ssid: String,
    pub captive_wifi_psk: String,

    pub latitude: f64,
    pub longitude: f64,
}

#[toml_cfg::toml_config]
struct Config {
    #[default("")]
    external_wifi_ssid: &'static str,
    #[default("")]
    external_wifi_psk: &'static str,

    #[default("pixel_board")]
    captive_wifi_ssid: &'static str,
    #[default("p1xel_board")]
    captive_wifi_psk: &'static str,

    #[default(DEFAULT_LATITUDE)]
    latitude: f64,
    #[default(DEFAULT_LONGITUDE)]
    longitude: f64,
}

impl Default for AppConfiguration {
    fn default() -> Self {
        // The constant `CONFIG` is auto-generated by `toml_config`.
        AppConfiguration {
            external_wifi_psk: CONFIG.external_wifi_psk.to_string(),
            external_wifi_ssid: CONFIG.external_wifi_ssid.to_string(),
            captive_wifi_psk: CONFIG.captive_wifi_psk.to_string(),
            captive_wifi_ssid: CONFIG.captive_wifi_ssid.to_string(),
            latitude: CONFIG.latitude,
            longitude: CONFIG.longitude,
        }
    }
}

pub static CURRENT_CONFIG: Lazy<Mutex<AppConfiguration>> =
    Lazy::new(|| Mutex::new(AppConfiguration::default()));

// Logs the config without passwords
pub fn print_current_censored_config() {
    let c = CURRENT_CONFIG.lock().unwrap().clone();
    let redacted = "REDACTED";
    print!("asdasd");
    println!(
        "Current config: captive_wifi_ssid: {:?}
    captive_wifi_psk: {:?}
    external_wifi_ssid: {:?}
    external_wifi_psk: {:?}
    latitude: {:?}
    longitude: {:?}",
        c.captive_wifi_ssid, redacted, c.external_wifi_ssid, redacted, c.latitude, c.longitude,
    );
}
